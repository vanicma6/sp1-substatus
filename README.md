# **Submission status table** 

## **Výběr knihovny**
Většina "tabulkových knihoven" zatím neplánuje podporu pro Vue3, nebo celkově není udržovaná, nebo není pod **MIT** licencí. Po podrobnější analýze jsem zvolil knihovnu [**Vue good table**](https://github.com/xaksis/vue-good-table).
### Knihovna plánuje **podporu** pro Vue3 a je **udržovaná**.

 
## **Knihovna umí:**
- Vyhledávat v tabulce
- Sortovat 
- Stránkovat 
- Filtrovat 


### [_**dokumentace**_](https://xaksis.github.io/vue-good-table/)
## **Ukázka tabulky**
 ![alt table](tableimg.png "Example table")
 - design **není finální**
 - Knihovna má odjímatelný css styl

 ## **Alternativní, podle mě horší varianty**
 - [**easytable**](https://github.com/Happy-Coding-Clans/vue-easytable) - pro mě nepřehledná dokumentace, **neplánovaná** podpora pro Vue3
 - [**vuetable2**](https://github.com/ratiw/vuetable-2) - **neudržovaná** 
 - [**matfish2/vuetable2**](https://github.com/matfish2/vue-tables-2) - **placená** MIT licence



